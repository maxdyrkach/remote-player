package sample;

import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaView;
import javafx.stage.FileChooser;
import javafx.util.Duration;
import main.java.playing.PlayingFileManager;
import main.java.playing.PlayingManager;

import java.io.File;
import java.util.List;


public class MediaController {


    @FXML
    private BorderPane borderPane;
    @FXML
    private MediaView mediaView;
    @FXML
    private ToolBar toolBar;
    @FXML
    private Button playBtn, pauseBtn, stopBtn, prevBtn, rewBtn, fwdBtn, nextBtn, openBtn;
    @FXML
    private Slider positionSlider, volumeSlider;
    @FXML
    private Label playTime, songLabel;
    @FXML
    Parent root;
    @FXML
    private ListView playlistView;

    //player:
    MediaPlayer mediaPlayer;
    private Duration duration;
    private PlayingManager playingManager;


    //node getters:
    public MediaView getMediaView() {
        return this.mediaView;
    }

    public BorderPane getBorderPane() {
        return this.borderPane;
    }

    public ToolBar getToolBar() {
        return toolBar;
    }

    public Button getPlayBtn() {
        return playBtn;
    }

    public void setMediaPlayer(MediaPlayer mediaPlayer) {
        this.mediaPlayer = mediaPlayer;
    }


    //Actions:
    public void onPressPlay(ActionEvent event) {

        playingManager.play();
        //mediaPlayer.play();


    }


    public void onPressPause(ActionEvent event) {
        playingManager.pause();
    }

    public Button getPauseBtn() {
        return pauseBtn;
    }

    public Button getStopBtn() {
        return stopBtn;
    }

    public Button getPrevBtn() {
        return prevBtn;
    }

    public Button getRewBtn() {
        return rewBtn;
    }

    public Button getFwdBtn() {
        return fwdBtn;
    }

    public Button getNextBtn() {
        return nextBtn;
    }

    public Button getOpenBtn() {
        return openBtn;
    }

    public PlayingManager getPlayingManager() {
        return playingManager;
    }

    public void onPressStop(ActionEvent event) {
        playingManager.stop();
    }


    public void onPressPrevious(ActionEvent event) {

    }

    public void onPressRewind(ActionEvent event) {

    }

    public void onPressForward(ActionEvent event) {

    }

    public void onPressNext(ActionEvent event) {

    }

    public void onPressOpen(ActionEvent event) {
        FileChooser chooser = new FileChooser();
        chooser.setInitialDirectory(new File("E:/My Downloads/Poets of the Fall - Ultraviolet (2018)"));
        chooser.setTitle("Open files:");
        chooser.setSelectedExtensionFilter(new FileChooser.ExtensionFilter("MP3", "*.mp3"));
        List<File> list = chooser.showOpenMultipleDialog(((Node) event.getSource()).getScene().getWindow());
        if (!list.isEmpty()) {
            playingManager.setPlaylist(PlayingFileManager.toPathList(list));
            playlistView.setItems(FXCollections.observableArrayList(list));
        }

    }

    public void onDoubleClickPlaylistPosition(MouseEvent event) {
        if (event.getEventType() == MouseEvent.MOUSE_CLICKED && event.getClickCount() == 2)
            playingManager.play(playlistView.getSelectionModel().getSelectedIndex());
    }

    public Slider getPositionSlider() {
        return positionSlider;
    }

    public Slider getVolumeSlider() {
        return volumeSlider;
    }

    public Parent getRoot() {
        return root;
    }

    public Duration getDuration() {
        return duration;
    }

    public Label getPlayTime() {
        return playTime;
    }

    public void setPlayingManager(PlayingManager playingManager) {
        this.playingManager = playingManager;
    }

    public ListView getPlaylistView() {
        return playlistView;
    }

    public Label getSongLabel() {
        return songLabel;
    }
}
