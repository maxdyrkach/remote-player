package sample;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import main.java.account.AccountManager;
import main.java.playing.PlayingManager;

public class Main extends Application {


    private PlayingManager playingManager;
    private AccountManager accountManager;
    private MediaController mediaController;


    @Override
    public void start(Stage primaryStage) throws Exception {

        /*MediaPlayer mediaPlayer = new MediaPlayer(new Media
                ("http://192.168.0.4:8080/getmp3?addr=/home/max/s.mp3"));*/
        //System.out.println(new File("E://My Downloads/Poets of the Fall - Ultraviolet (2018)/02. My Dark Disquiet.mp3").toURI().toString());


       /* new Thread(() -> {
            Thread.currentThread().setDaemon(true);

            try {
                //ServerSocket serverSocket = new ServerSocket(9010);
                ServerSocket serverSocket = new ServerSocket (8080);
                URI uri = new File("E:/My Downloads/Poets of the Fall - Ultraviolet (2018)/02. My Dark Disquiet.mp3").toPath().toUri();


                PrintWriter printWriter = new PrintWriter(new OutputStreamWriter(socket.getOutputStream()),true);
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));

                while(true){

                }


            } catch (IOException e) {
                e.printStackTrace();
            }


        }).start();*/

        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("sample2.fxml"));
        Parent root = fxmlLoader.load();
        mediaController = fxmlLoader.getController();//controller


        playingManager = PlayingManager.getInstance();
        playingManager.setMediaController(mediaController);
        mediaController.setPlayingManager(playingManager);
        //Media  media = new Media(new File("E:/My Downloads/Poets of the Fall - Ultraviolet (2018)/02. My Dark Disquiet.mp3").toURI().toString());
        //MediaPlayer mediaPlayer = new MediaPlayer(media);

       /* mediaController.getMediaView().setMediaPlayer(mediaPlayer);
        mediaPlayer.setOnError(() -> {
            System.out.println(mediaPlayer.getError().toString());
        });
        mediaPlayer.play();*/


        //playingManager.setMediaPlayer(mediaPlayer);
        //mediaController.setMediaPlayer(mediaPlayer);
        primaryStage.setTitle("Remote Player");
        primaryStage.setResizable(false);
        primaryStage.setScene(new Scene(root, 500, 650));

        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
