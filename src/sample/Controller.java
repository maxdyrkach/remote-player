package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.FileChooser;
import main.java.playing.PlayingManager;

import java.io.File;
import java.nio.file.Path;


public class Controller {

    private PlayingManager playingManager = PlayingManager.getInstance();
    private Path path;

    @FXML
    Button playBtn, stopBtn, chooseBtn, pauseBtn;
    @FXML
    TextField pathField;

    public void choose(ActionEvent event) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setSelectedExtensionFilter(new FileChooser.ExtensionFilter("Mp3 files", "*.mp3"));
        fileChooser.setInitialDirectory(new File("E:\\My Downloads\\Poets of the Fall - Ultraviolet (2018)"));
        path = fileChooser.showOpenDialog(((Node) event.getSource()).getScene().getWindow()).toPath();
        pathField.setText(path.toString());
    }

    public void play(ActionEvent event) {
        playingManager.play();
    }

    public void stop(ActionEvent event) {
        playingManager.stop();

    }

    public void pause(ActionEvent event) {
        playingManager.pause();
    }

    public void changePathField(ActionEvent event) {
        if (event.getSource().getClass().equals(TextField.class)) {
            setPath(((TextField) event.getSource()).getText());
        }
    }

    public void setPath(String string) {

        path = new File(string).toPath();

    }

}
