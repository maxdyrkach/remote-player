package main.java.account;

import java.net.Socket;

public interface Account {

    Account getAccount();

    String getId();

    String getLogin();

    String hash();

    Socket getSocket();

    void setLogin();

    void setSocket();


}
