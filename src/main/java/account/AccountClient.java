package main.java.account;

import java.net.Socket;

interface AccountClient {

    void connect(Socket socket);

    boolean checkAccount(Account account);

    Account sendNewAccont(Account account);

    Account changeAccount(Account account, String password);


}
