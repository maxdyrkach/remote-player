package main.java.account;


public class AccountManager {
    private static AccountManager accountManager;

    private AccountManager() {
        accountManager = this;
    }

    public static AccountManager getInstance() {
        synchronized (accountManager) {
            if (accountManager != null) {
                return accountManager;
            } else
                return new AccountManager() {
                };
        }
    }


}
