package main.java.playing;

import java.io.File;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;

class PlayingFileManagerImpl implements PlayingFileManager {

    private PlayingManager playingManager;

    PlayingFileManagerImpl(PlayingManager playingManager) {
        this.playingManager = playingManager;
    }

    public static List<Path> toPathList(List<File> list) {
        return list.stream().map((x) -> x.toPath()).collect(Collectors.toList());
    }
}
