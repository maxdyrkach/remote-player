package main.java.playing;

import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.util.Duration;
import sample.MediaController;

import java.nio.file.Path;
import java.util.List;

public class PlayingManager {


    private static PlayingManager playingManager = new PlayingManager();

    private PlayingProcess playingProcess;
    private PlayingClient playingClient;
    private PlayingFileManager playingFileManager;

    private Media media;
    private MediaPlayer.Status playerStatus;
    private Slider positionSlider;
    private Slider volumeSlider;
    private Button playBtn;
    private MediaPlayer mediaPlayer;
    private Duration duration;
    private Label playTime;

    private List<Path> currentPlaylist;

    private boolean repeat = false;
    private boolean stopRequested = false;
    private boolean atEndOfMedia = false;

    private MediaController mediaController;

    private PlayingManager() {
        playingProcess = new PlayingProcessImpl(this);
        playingClient = new PlayingClientImpl(this);
        playingFileManager = new PlayingFileManagerImpl(this);
    }


    public void play() {

        playingProcess.setMediaController(mediaController);
        //Path path = new File("E:/My Downloads/Poets of the Fall - Ultraviolet (2018)/02. My Dark Disquiet.mp3").toPath();
        //Path path = currentPlaylist.get(0);

        playingProcess.play();

    }

    public void play(int position) {
        playingProcess.setMediaController(mediaController);
        playingProcess.setPosition(position);
        playingProcess.play();
    }

    public void stop() {
        playingProcess.stop();
    }

    public void pause() {
        playingProcess.pause();
    }

    /*public void setPlaylist (List<File> list){
        setPlaylist(playingFileManager.toPathList(list));


    }*/

    public void setPlaylist(List<Path> list) {
        this.currentPlaylist = list;
        playingProcess.setPlaylist(list);
    }

    public void setMediaController(MediaController mediaController) {
        this.mediaController = mediaController;
    }

    public void setMediaPlayer(MediaPlayer mediaPlayer) {
        this.mediaPlayer = mediaPlayer;
    }

    public static synchronized PlayingManager getInstance() {

        if (playingManager != null)
            return playingManager;
        else
            return new PlayingManager();

    }


}
