package main.java.playing;

import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import sample.MediaController;

import java.nio.file.Path;
import java.util.List;

public interface PlayingProcess {

    int play();

    int play(int track);

    int play(Path path);

    int play(Media media);

    int stop();

    int pause();

    int next();

    int previous();

    boolean hasNext();

    boolean hasPrevious();

    boolean isPlaying();

    int setPosition(int position);

    int getPosition();

    int setVolume(int volume);

    double getVolume();

    List setPlaylist(List<Path> playlist);

    List getPlaylist();

    Track getCurrentTrack();

    void setMediaController(MediaController controller);

    void setMediaPlayer(MediaPlayer mediaPlayer);

}
