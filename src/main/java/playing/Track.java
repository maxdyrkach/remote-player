package main.java.playing;

import java.net.URI;
import java.nio.file.Path;


public abstract class Track {
    private int id;
    private URI uri;
    private Path file;
    private String artist;
    private String song;

    public Track(int id, URI uri, Path file) {
        this.id = id;
        this.uri = uri;
        this.file = file;
    }

    int getId() {
        return id;
    }

    URI getUri() {
        return uri;
    }

    Path getFile() {
        return file;
    }

    String getArtist() {
        return artist;
    }

    String getSong() {
        return song;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public void setSong(String song) {
        this.song = song;
    }
}
