package main.java.playing;

import javafx.application.Platform;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.util.Duration;
import sample.MediaController;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

class PlayingProcessImpl implements PlayingProcess {

    PlayingProcessImpl(PlayingManager playingManager) {
        this.playingManager = playingManager;
    }

    private PlayingManager playingManager;
    private boolean repeat = false;
    private boolean repeatPlaylist = false;

    private boolean stopRequested = false;
    private boolean atEndOfMedia = false;
    private boolean atEndOfPlaylist = false;

    private MediaPlayer.Status playerStatus;
    private MediaPlayer mediaPlayer;
    private List<Media> playlist = new ArrayList<>();
    //private List<Path> playlist = new ArrayList<>();
    //private Media media;
    private Path currentPlayingPath;
    private Media currentPlayingMedia;
    private int currentPosition;


    private Duration duration;
    private double volumeValue = 0.5D;

    private MediaController mediaController;
    private Button playBtn;
    private Slider positionSlider;
    private Label playTime;
    private Slider volumeSlider;


    @Override
    public synchronized int play(Media outerMedia) {


        if (mediaPlayer == null || !currentPlayingMedia.equals(outerMedia)) {
            if (mediaPlayer != null)
                mediaPlayer.stop();

            this.currentPlayingMedia = outerMedia;
            //
            //mediaController.getSongLabel().setText(media.getMetadata().);
            mediaPlayer = new MediaPlayer(currentPlayingMedia);
            duration = currentPlayingMedia.getDuration();
            setCycleCount();

            mediaPlayer.setOnError(() -> System.err.println(mediaPlayer.getError()));
            currentPlayingMedia.setOnError(() -> System.err.println(currentPlayingMedia.getError()));

            mediaPlayer.setOnReady(() -> {
                duration = currentPlayingMedia.getDuration();
                updateValues();
            });

            mediaPlayer.setOnStopped(() -> positionSlider.setValue(0D));

            mediaPlayer.setOnPlaying(() -> {
                if (stopRequested) {
                    pause();
                    stopRequested = false;
                } else {
                    playBtn.setText("||");
                }
            });

            mediaPlayer.setOnPaused(() -> {
                System.out.println("onPaused");
                playBtn.setText("Play");
            });

            mediaPlayer.currentTimeProperty().addListener(ov -> updateValues());
            positionSlider.valueProperty().addListener(ov -> {
                if (positionSlider.isValueChanging() || positionSlider.isPressed()) {
                    // multiply duration by percentage calculated by slider position
                    mediaPlayer.seek(duration.multiply(positionSlider.getValue() / 100.0));
                }
            });


            mediaPlayer.setOnEndOfMedia(() -> {
                if (!repeat) {
                    playBtn.setText(">");
                    stopRequested = true;
                    System.out.println(stopRequested);
                    atEndOfMedia = true;
                    System.out.println(atEndOfMedia);
                    if (hasNext()) {
                        mediaPlayer.stop();
                        currentPosition++;
                        play(playlist.get(currentPosition));
                        //setVolume(volumeSlider.getValue());
                        atEndOfMedia = false;
                        stopRequested = false;
                    } else
                        atEndOfPlaylist = true;

                }
            });

            mediaPlayer.volumeProperty().addListener((ev) -> {
                setVolume(mediaPlayer.getVolume());
                System.out.println(getVolume());
            });

            positionSlider.setMin(0);
            positionSlider.setMax(100);
            positionSlider.setBlockIncrement(0.1D);

            volumeSlider.setMax(1D);
            volumeSlider.setBlockIncrement(0.01D);
            mediaPlayer.volumeProperty().bind(volumeSlider.valueProperty());
            volumeSlider.setValue(getVolume());
            //volumeSlider.setValue(volumeValue);


        }

        playerStatus = mediaPlayer.getStatus();
        System.out.println(playerStatus);


        if (playerStatus == MediaPlayer.Status.HALTED)
            return -1;
        if (playerStatus == MediaPlayer.Status.PAUSED
                || playerStatus == MediaPlayer.Status.READY
                || playerStatus == MediaPlayer.Status.STOPPED
                || playerStatus == MediaPlayer.Status.UNKNOWN) {

            mediaPlayer.play();

        } else {
            mediaPlayer.pause();
        }
        if (atEndOfMedia && playerStatus == MediaPlayer.Status.PLAYING) {
            mediaPlayer.seek(mediaPlayer.getStartTime());
            atEndOfMedia = false;
        }

        return 0;
    }

    private void updateValues() {
        if (playTime != null && positionSlider != null) {
            Platform.runLater(() -> {
                Duration currentTime = mediaPlayer.getCurrentTime();
                playTime.setText(formatTime(currentTime, duration));
                positionSlider.setDisable(duration.isUnknown());
                if (!positionSlider.isDisabled()
                        && duration.greaterThan(Duration.ZERO)
                        && !positionSlider.isValueChanging()) {
                    positionSlider.setValue(currentTime.toMillis() / duration.toMillis()
                            * 100.0);
                }
            });
        }
    }

    private static String formatTime(Duration elapsed, Duration duration) {
        int intElapsed = (int) Math.floor(elapsed.toSeconds());
        int elapsedHours = intElapsed / (60 * 60);
        if (elapsedHours > 0) {
            intElapsed -= elapsedHours * 60 * 60;
        }
        int elapsedMinutes = intElapsed / 60;
        int elapsedSeconds = intElapsed - elapsedHours * 60 * 60
                - elapsedMinutes * 60;

        if (duration.greaterThan(Duration.ZERO)) {
            int intDuration = (int) Math.floor(duration.toSeconds());
            int durationHours = intDuration / (60 * 60);
            if (durationHours > 0) {
                intDuration -= durationHours * 60 * 60;
            }
            int durationMinutes = intDuration / 60;
            int durationSeconds = intDuration - durationHours * 60 * 60 -
                    durationMinutes * 60;
            if (durationHours > 0) {
                return String.format("%d:%02d:%02d/%d:%02d:%02d",
                        elapsedHours, elapsedMinutes, elapsedSeconds,
                        durationHours, durationMinutes, durationSeconds);
            } else {
                return String.format("%02d:%02d/%02d:%02d",
                        elapsedMinutes, elapsedSeconds, durationMinutes,
                        durationSeconds);
            }
        } else {
            if (elapsedHours > 0) {
                return String.format("%d:%02d:%02d", elapsedHours,
                        elapsedMinutes, elapsedSeconds);
            } else {
                return String.format("%02d:%02d", elapsedMinutes,
                        elapsedSeconds);
            }
        }
    }

    @Override
    public int play(int track) {
        return 0;
    }

    @Override
    public int play() {
        if (playlist != null) {
            play(playlist.get(currentPosition));
        }

        return 0;
    }

    @Override
    public int play(Path path) {
        Media media = new Media(path.toFile().toURI().toString());
        play(media);

        return 0;
    }

    @Override
    public int stop() {
        if (mediaPlayer != null) {
            mediaPlayer.stop();
            playBtn.setText("Play");

        }
        return 0;
    }

    @Override
    public int pause() {
        mediaPlayer.pause();
        return 0;
    }

    @Override
    public int next() {
        if (mediaPlayer != null) {
            stop();
            if (hasNext()) {
                setPosition(currentPosition++);
            } else if (repeatPlaylist)
                setPosition(0);
        }

        return 0;
    }

    @Override
    public int previous() {
        return 0;
    }

    @Override
    public boolean hasNext() {
        return currentPosition < playlist.size() - 1;
    }

    @Override
    public boolean hasPrevious() {
        return false;
    }

    @Override
    public boolean isPlaying() {
        return false;
    }

    @Override
    public int setPosition(int position) {
        if (!(position < 0)) {
            currentPosition = position;
            return position;
        } else return -1;
    }

    @Override
    public int getPosition() {
        return 0;
    }

    @Override
    public int setVolume(int volume) {
        return 0;
    }

    @Override
    public double getVolume() {
        return this.volumeValue;
    }

    @Override
    public List setPlaylist(List<Path> playlist) {
        this.playlist = playlist.stream().map((x) -> new Media(x.toFile().toURI().toString())).collect(Collectors.toList());
        //this.playlist = playlist;
        return this.playlist;
    }


    @Override
    public List getPlaylist() {
        return null;
    }

    @Override
    public Track getCurrentTrack() {
        return null;
    }

    public void setMediaPlayer(MediaPlayer mediaPlayer) {
        this.mediaPlayer = mediaPlayer;
    }

    public void setMediaController(MediaController mediaController) {
        this.mediaController = mediaController;
        this.playBtn = mediaController.getPlayBtn();
        this.positionSlider = mediaController.getPositionSlider();
        this.playTime = mediaController.getPlayTime();
        this.volumeSlider = mediaController.getVolumeSlider();


    }


    public void setVolume(double volumeValue) {
        this.volumeValue = volumeValue;
    }

    public void setPlayingManager(PlayingManager playingManager) {
        this.playingManager = playingManager;
    }

    public void setRepeat(boolean repeat) {
        this.repeat = repeat;
        setCycleCount();

    }

    protected int setCycleCount() {
        if (mediaPlayer != null) {
            mediaPlayer.setCycleCount(repeat ? MediaPlayer.INDEFINITE : 1);
            return 0;
        } else
            return -1;
    }

}
